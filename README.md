pySpread
====================

**Installation**


See INSTALL file for installation and COPYING file for license

- Homepage: https://pyspread.gitlab.io/
- Repository: https://gitlab.com/pyspread/pyspread
- API Docs: https://pyspread.gitlab.io/pyspread/


**How to start hacking**

Fork the github repo at `https://gitlab.com/pyspread/pyspread.git`

In order to start pyspread without installation run
```
$ ./pyspread.sh
```
inside the top directory.

Commit your changes, push them into your fork and send a pull request.

This page gives an overview how to do this:
- https://help.github.com/articles/fork-a-repo

You can find more more details about code organization at
- https://pyspread.gitlab.io/pyspread/