API
====================================


.. toctree::
    :maxdepth: 3

    actions.rst
    commands.rst
    dialogs.rst
    entryline.rst
    grid.rst
    icons.rst
    menubar.rst
    panels.rst
    pyspread.rst
    settings.rst
    toolbar.rst
    widgets.rst
    workflows.rst
    model/index.rst
    lib/index.rst
    interfaces/index.rst

